#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your name");
MODULE_DESCRIPTION("PMUON");

int init_module(void) {
    unsigned int v;
    printk("Turn PMU on\n");

    // 1. Enable "User Enable Register"
    asm volatile("mcr p15, 0, %0, c9, c14, 0\n\t" :: "r" (0x00000001)); 

    // 2. Reset Performance Monitor Control Register(PMCR), Count Enable Set Register, and Overflow Flag Status Register
    asm volatile ("mcr p15, 0, %0, c9, c12, 0\n\t" :: "r"(0x00000017));
    asm volatile ("mcr p15, 0, %0, c9, c12, 1\n\t" :: "r"(0x8000003f));
    asm volatile ("mcr p15, 0, %0, c9, c12, 3\n\t" :: "r"(0x8000003f));

    // 3. Disable Interrupt Enable Clear Register
    asm volatile("mcr p15, 0, %0, c9, c14, 2\n\t" :: "r" (~0));

    // 4. Read how many event counters exist 
    asm volatile("mrc p15, 0, %0, c9, c12, 0\n\t" : "=r" (v)); // Read PMCR
    printk("pmon_init(): have %d configurable event counters.\n", (v >> 11) & 0x1f);

    // 5. Set event counter registers 
    // ***** YOUR CODE STARTS HERE *****
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000000));
    // Assign L1D_CACHE L1 Data cache access.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x00000004));
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000001));
    // Assign 0x03  L1D_CACHE_REFILL L1 Data cache refill.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x00000003));
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000002));
    // Assign 0x16  L2D_CACHE L2 Data cache access.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x00000016));
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000003));
    // Assign 0x17  L2D_CACHE_REFILL L2 Data cache refill.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x00000017));
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000004));
    // Assign 0xC9 Conditional branch executed.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x000000c9));
    // Select 
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000005));
    // Assign 0xCC Conditional branch mispredicted.
    asm volatile ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(0x000000cc));

    // ***** YOUR CODE ENDS HERE *******
    return 0;
}

void cleanup_module(void) {
    printk("GOODBYE, PMU Off\n");
}


