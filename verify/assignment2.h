#ifndef _ASSIGNMENT2_BODY_
#define _ASSIGNMENT2_BODY_

#include "scheduler.h"

struct shared_variable; // Defined in assignment1.h

// Call at the start part of the program before actual scheduling
void learn_workloads(struct shared_variable* sv);

// Call in the scheduler thread
TaskSelection select_task(struct shared_variable* sv, const int* aliveTasks, long long idleTime);

extern long long over_all_idle;
extern long long over_all_time[2];
typedef enum
{
    button,
    two_color,
    temp,
    track,
    touch,
    rgb,
    aled,
    buzzer
}thread_id;

typedef struct
{
    long long deadline;
    unsigned long exec_time;
    int freq;
    thread_id id;
    long long dynamic_deadline;

}thread_profile;

static inline void read_cpu_cycles(unsigned long *count)
{
    asm volatile("mrc p15, 0, %0, c9, c13, 0\n\t" : "=r" (*count));

}
// L1D_CACHE
static inline void read_reg1(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000000));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

// L1D_CACHE_REFILL
static inline void read_reg2(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000001));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

// L2D_CACHE
static inline void read_reg3(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000002));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

// L2D_CACHE_REFILL
static inline void read_reg4(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000003));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

static inline void read_reg5(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000004));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

static inline void read_reg6(unsigned long *count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000005));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (*count));
}

static inline void reset_counters()
{
    // Reset all cycle counter and event counters
    asm volatile ("mcr p15, 0, %0, c9, c12, 0\n\t" :: "r"(0x00000017));
}

#endif
