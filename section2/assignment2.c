#include "assignment1.h"
#include "assignment2.h"
#include "workload.h"
#include "scheduler.h"
#include "governor.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#define NUM_TASKS 8

unsigned long exec_time[NUM_TASKS] = {0};
long long exec_time_funct[NUM_TASKS] = {0};
static thread_profile threads[NUM_TASKS];
unsigned long count;
long long over_all_idle = 0;
long long over_all_time[2] = {0};
long long temp_var;
int flag_start = 0;
int last_freq;
int comp (const void * elem1, const void * elem2)
{
    thread_profile f = *((thread_profile*)elem1);
    thread_profile s = *((thread_profile*)elem2);
    if (f.deadline < s.deadline) return -1;
    if (f.deadline > s.deadline) return 1;
    return 0;
}

int comp_priority (const void * elem1, const void * elem2)
{
    thread_profile f = *((thread_profile*)elem1);
    thread_profile s = *((thread_profile*)elem2);
    if (f.dynamic_deadline < s.dynamic_deadline) return -1;
    if (f.dynamic_deadline > s.dynamic_deadline) return 1;
    return 0;
}
// Assignment: You need to implement the following two functions.

// learn_workloads(SharedVariable* v):
// This function is called at the start part of the program before actual scheduling
// - Parameters
// sv: The variable which is shared for every function over all threads
void learn_workloads(SharedVariable* sv) {
    // TODO: Fill the body
    // This function is executed before the scheduling simulation.
    // You need to calculate the execution time of each thread here.
    init_shared_variable(sv);
    int i;
    int N = 100;
    int var = (int)sv->current_state;
    printf("learn workloads\n");
    fflush(stdout);
    set_by_min_freq();
    unsigned long cache_miss_l1[NUM_TASKS] = {0};
    unsigned long count_l1, count_l2;
    unsigned long cache_miss_l2[NUM_TASKS] = {0};
    long long time_us = get_current_time_us();
    for (i = 0; i < N; ++i)
    {
        /* code */

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_twocolor(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)two_color] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)two_color] += count_l1;
        cache_miss_l2[(int)two_color] += count_l2;
        exec_time[(int)two_color] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_temp(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)temp] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)temp] += count_l1;
        cache_miss_l2[(int)temp] += count_l2;
        exec_time[(int)temp] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_track(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)track] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)track] += count_l1;
        cache_miss_l2[(int)track] += count_l2;
        exec_time[(int)track] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_aled(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)aled] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)aled] += count_l1;
        cache_miss_l2[(int)aled] += count_l2;
        exec_time[(int)aled] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_button(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)button] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)button] += count_l1;
        cache_miss_l2[(int)button] += count_l2;
        exec_time[(int)button] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_touch(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)touch] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)touch] += count_l1;
        cache_miss_l2[(int)touch] += count_l2;
        exec_time[(int)touch] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_rgbcolor(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)rgb] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)rgb] += count_l1;
        cache_miss_l2[(int)rgb] += count_l2;
        exec_time[(int)rgb] += count;
        count = 0;

        sv->current_state = ((var + 1)%5);
        count_l1 = count_l2 = 0;
        time_us = get_current_time_us();
        reset_counters();
        thread_buzzer(sv);
        read_cpu_cycles(&count);
        read_reg2(&count_l1);
        read_reg4(&count_l2);
        exec_time_funct[(int)buzzer] += (get_current_time_us() - time_us);
        cache_miss_l1[(int)buzzer] += count_l1;
        cache_miss_l2[(int)buzzer] += count_l2;
        exec_time[(int)buzzer] += count;
        count = 0;
        var++;

    }

    for (i = 0; i < NUM_TASKS; ++i)
    {
        double Heuristic;
        exec_time[i] = exec_time[i] / N;
        printf("Profiling task %d : %lf : %lf : L1 L2 misses: %lu : %lu \n", i,
            exec_time[i]/600.0, exec_time[i]/1200.0, cache_miss_l1[i]/ (N), cache_miss_l2[i]/(N));
        Heuristic = (cache_miss_l1[i]/ (N) + cache_miss_l2[i]/(N))/ ((exec_time[i]/600)*(1.0));
        double inverse_heuristic = 1.0/Heuristic;
        printf("Time :%d :: %lld\n", i, exec_time_funct[i]/N);
        printf("Heuristic value %lf : %lf \n", Heuristic, 1.0/Heuristic);
        threads[i].exec_time = exec_time[i]/1200;
        //threads.freq = (threads[i].exec_time * 600) > (threads[i].exec_time * 900)
        threads[i].id = i;
        threads[i].deadline = workloadDeadlines[i];
        threads[i].dynamic_deadline = 0;
        //if (Heuristic > 1.0)
        if (inverse_heuristic/10 > 1.0)
            threads[i].freq = 1;
        else
            threads[i].freq = 0;

    }
    set_by_max_freq();
    init_shared_variable(sv);
    init_sensors(sv);
}


// select_task(SharedVariable* sv, const int* aliveTasks):
// This function is called while runnning the actual scheduler
// - Parameters
// sv: The variable which is shared for every function over all threads
// aliveTasks: an array where each element indicates whether the corresponed task is alive(1) or not(0).
// idleTime: a time duration in microsecond. You can know how much time was waiting without any workload
//           (i.e., it's larger than 0 only when all threads are finished and not reache the next preiod.)
// - Return value
// TaskSelection structure which indicates the scheduled task and the CPU frequency
TaskSelection select_task(SharedVariable* sv, const int* aliveTasks, long long idleTime) {

    // Tip 1. You may get the current time elapsed in the scheduler here like:
    // long long curTime = get_scheduler_elapsed_time_us();
    over_all_time[last_freq] += ((get_current_time_us()- temp_var - idleTime)*flag_start);
    flag_start = 1;
    TaskSelection sel;
    sel.task = 0;
    sel.freq = 0;
    static int prev_selection = 1;
    static int alive_hist[NUM_TASKS] = {0};
    int i;
    over_all_idle += idleTime;
    long long time_us = get_current_time_us();
    for (i = 0; i < NUM_TASKS; ++i){
        threads[i].dynamic_deadline = ( time_us + threads[i].deadline)*aliveTasks[threads[i].id]*(alive_hist[threads[i].id] ^ 0x1)
            + threads[i].dynamic_deadline*aliveTasks[threads[i].id]*alive_hist[threads[i].id];
        alive_hist[threads[i].id] = aliveTasks[threads[i].id];
    }

    qsort (threads, sizeof(threads)/sizeof(*threads), sizeof(*threads), comp_priority);


    for (i = 0; i < NUM_TASKS; ++i){
        if(aliveTasks[threads[i].id]){
            sel.task = threads[i].id;
            sel.freq = threads[i].freq;
            prev_selection = threads[i].id;
            last_freq = threads[i].freq;
            break;
        }
    }
    temp_var = get_current_time_us();
    return sel;
}
