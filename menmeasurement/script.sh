#increasing size
sudo ./memmeasurement_cp 10000 128 256
sudo ./memmeasurement_cp 10000 128 512
sudo ./memmeasurement_cp 10000 128 1024
sudo ./memmeasurement_cp 10000 128 2048
sudo ./memmeasurement_cp 10000 128 4096
sudo ./memmeasurement_cp 10000 128 8192
sudo ./memmeasurement_cp 10000 128 16384
sudo ./memmeasurement_cp 10000 128 32768
sudo ./memmeasurement_cp 10000 128 65536
sudo ./memmeasurement_cp 10000 128 131072
sudo ./memmeasurement_cp 10000 128 262144
sudo ./memmeasurement_cp 10000 128 524288
sudo ./memmeasurement_cp 10000 128 1048576
#increasing stride
sudo ./memmeasurement_cp 10000 8 1048576
sudo ./memmeasurement_cp 10000 16 1048576
sudo ./memmeasurement_cp 10000 32 1048576
sudo ./memmeasurement_cp 10000 64 1048576
sudo ./memmeasurement_cp 10000 128 1048576
sudo ./memmeasurement_cp 10000 256 1048576
sudo ./memmeasurement_cp 10000 512 1048576
sudo ./memmeasurement_cp 10000 1024 1048576
#increasing iterations
sudo ./memmeasurement_cp 5000 128 1048576
sudo ./memmeasurement_cp 10000 128 1048576
sudo ./memmeasurement_cp 15000 128 1048576
sudo ./memmeasurement_cp 20000 128 1048576
sudo ./memmeasurement_cp 25000 128 1048576
sudo ./memmeasurement_cp 30000 128 1048576
sudo ./memmeasurement_cp 35000 128 1048576
sudo ./memmeasurement_cp 40000 128 1048576
sudo ./memmeasurement_cp 45000 128 1048576
sudo ./memmeasurement_cp 50000 128 1048576



