#include <wiringPi.h>
#include <stdio.h>
#include "assignment1.h"

 
#define  RED_MID        0
#define  GREEN_RIGHT    1

void body_twocolor(SharedVariable* sv) {
  digitalWrite(RED_MID, HIGH);   //led off
  printf("led RED...\n");
  delay(500);
  digitalWrite(RED_MID, LOW);   //led off
  delay(5);
  digitalWrite(GREEN_RIGHT, HIGH);  //led on
  printf("...led GREEN\n");
  delay(500);
  digitalWrite(GREEN_RIGHT, LOW);   //led off
  delay(5);
}
 
int main(void)
{
  if(wiringPiSetup() == -1){
    //if the wiringPi initialization fails, print error message
    printf("setup wiringPi failed !");
    return 1;
  }
 
  pinMode(RED_MID, OUTPUT);
  pinMode(GREEN_RIGHT, OUTPUT);
 
  while(1){
    body_twocolor(NULL);

    // digitalWrite(LEDPin, LOW);   //led off
    // printf("led off...\n");
    // delay(500);
    // digitalWrite(LEDPin, HIGH);  //led on
    // printf("...led on\n");
    // delay(500);
  }
  return 0;
}

