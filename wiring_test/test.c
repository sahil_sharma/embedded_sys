#include <wiringPi.h>
#include <stdio.h>
#include "assignment1.h"
#include "measure.h"
#include <softPwm.h>

 
#define PIN_BUTTON 0

// 2. 2-Color LED
#define PIN_YELLOW 1

// 3. Temperature
#define PIN_TEMP 4

// 4. Tracking Sensor
#define PIN_TRACK 5

// 5. Touch Sensor
#define PIN_TOUCH 6

// 6. RGB(3-Color) LED
#define PIN_RED 7
#define PIN_GREEN 8
#define PIN_BLUE 9

// 7. Auto-flash Red
#define PIN_ALED 12

// 8. Buzzer
#define PIN_BUZZER 13

#include "assignment1.h"
#include <stdio.h>
#include <wiringPi.h>
#include <softPwm.h>
#include <stdint.h>
unsigned long total = 0;
unsigned long count = 0;

void init_shared_variable(SharedVariable* sv) {
    // You can initialize the shared variable if needed.
    sv->bProgramExit = 0;
    sv->button_state = 1;
    sv->touch_count = 10;
    sv->current_state = OFF;
}

void init_sensors(SharedVariable* sv) {
    pinMode(PIN_BUTTON, INPUT);
    pinMode(PIN_TEMP, INPUT);
    pinMode(PIN_TRACK, INPUT);
    pinMode(PIN_TOUCH, INPUT);

    pinMode(PIN_YELLOW, OUTPUT);
    pinMode(PIN_RED, OUTPUT);
    pinMode(PIN_GREEN, OUTPUT);
    pinMode(PIN_BLUE, OUTPUT);
    pinMode(PIN_ALED, OUTPUT);
    pinMode(PIN_BUZZER, OUTPUT);
    
    digitalWrite(PIN_YELLOW,LOW);    
    digitalWrite(PIN_RED,LOW);    
    digitalWrite(PIN_GREEN,LOW);    
    digitalWrite(PIN_BLUE,LOW);    
    digitalWrite(PIN_ALED,LOW);    
    digitalWrite(PIN_BUZZER,LOW);    

    softPwmCreate(PIN_RED, 0, 0xFF);
    softPwmCreate(PIN_GREEN, 0, 0xFF);
    softPwmCreate(PIN_BLUE, 0, 0xFF);

}

void body_button(SharedVariable* sv) {    
    //printf("STATE %d \n", sv->current_state);
    if(1){
        //printf("BUTTON %d %d\n", digitalRead(PIN_BUTTON), sv->current_state);
        if(sv->button_state == 1)
            if(sv->current_state == OFF){
                sv->current_state = DRIVE;
                sv->button_state = 0;
            }
            else{
                sv->current_state = OFF;
                sv->button_state = 0;
            }
    }
    else
        sv->button_state = 1;
    //delay(100);
}

void body_twocolor(SharedVariable* sv) {
    
    switch(sv->current_state){
        case OFF:
        digitalWrite(PIN_YELLOW, LOW);
           break;
        case DRIVE:
        digitalWrite(PIN_YELLOW, HIGH);
        break;
        default:
        break;
    }
}

void body_temp(SharedVariable* sv) {
    
    digitalRead(PIN_TEMP);
    if(sv->current_state == DRIVE || sv->current_state == E1){
        if(digitalRead(PIN_TEMP)){
            //printf("TEMP ON\n");
            sv->current_state = E1;
        } else {
            //printf("TEMP OFF\n");
            sv->current_state = DRIVE;
        }
    }
}

void body_track(SharedVariable* sv) {
       
    digitalRead(PIN_TEMP);
    if((sv->current_state == DRIVE || sv->current_state == E1)){
        //printf("TRACK ON\n");
        sv->current_state = E2;
    }
}

void body_touch(SharedVariable* sv) {
    
    digitalRead(PIN_TEMP);
    if(( sv->current_state == DRIVE || sv->current_state == E1 || sv->current_state == E3)){
        //printf("TOUCH ON\n");
        sv->current_state = E3;
        sv->touch_count = 100;
    }
    if(!digitalRead(PIN_TOUCH))
        sv->touch_count--;       
}

void body_rgbcolor(SharedVariable* sv) {

      
    switch(sv->current_state){
        case OFF:
            //Set RGB BLUE
            softPwmWrite(PIN_RED, 0x0);
            softPwmWrite(PIN_GREEN, 0x0);
            softPwmWrite(PIN_BLUE, 0xFF);
        break;
        case DRIVE:
            //Set to Yellow
            softPwmWrite(PIN_RED, 0xFF);
            softPwmWrite(PIN_GREEN, 0xFF);
            softPwmWrite(PIN_BLUE, 0x0);
        break;
        case E2:
            //Set RGB PIN_RED
            softPwmWrite(PIN_RED, 0xFF);
            softPwmWrite(PIN_GREEN, 0x0);
            softPwmWrite(PIN_BLUE, 0x0);
        break;
        case E3:
            //Set RGB PURPLE
            softPwmWrite(PIN_RED, 0xC8);
            softPwmWrite(PIN_GREEN, 0x3B);
            softPwmWrite(PIN_BLUE, 0xFF);
        break;
        default:
        break;
    }
    
}

void body_aled(SharedVariable* sv) {
    
    switch(sv->current_state){
        case OFF:
            //Set  OFF
        digitalWrite(PIN_ALED, LOW);
        break;
        case E1:
        digitalWrite(PIN_ALED, HIGH);
        break;
        default:
        digitalWrite(PIN_ALED, LOW);
        break;
    }
}

void body_buzzer(SharedVariable* sv) {
    
    switch(sv->current_state){
        case OFF:
            //Set Buzzer OFF
        digitalWrite(PIN_BUZZER, LOW);
        break;
        case E3:
        if(sv->touch_count > 0)
            digitalWrite(PIN_BUZZER, HIGH);
        else
            digitalWrite(PIN_BUZZER, LOW);
        break;
        default:
        break;
    }
}

int N = 100000;

int main(void)
{
  if(wiringPiSetup() == -1){
    //if the wiringPi initialization fails, print error message
    printf("setup wiringPi failed !");
    return 1;
  }
 
  init_sensors(NULL);
  char szOldGovernor[32];
  set_governor("userspace", szOldGovernor);
  set_by_min_freq(); // You can also test the minimum frequency using "set_by_min_freq()"
  //pinMode(TEMP, INPUT);
  //pinMode(TRACK, INPUT);
 //pinMode(TOUCH, INPUT);
 // pinMode(BUTTON, INPUT);
  SharedVariable sv;
  init_shared_variable(&sv);
  int i;

  for (i = 0; i < N; ++i)
  {
    /* code */
    sv.current_state = (((int)sv.current_state + 1)%5);
    //body_twocolor(NULL);
    //body_temp(NULL);
    //body_track(NULL);
    //body_aled(NULL);
    //body_button(NULL);
    reset_counters();
    body_buzzer(&sv);
    read_cpu_cycles(&count);
    total += count;
    count = 0;
    // digitalWrite(LEDPin, LOW);   //led off
    // printf("led off...\n");
    // delay(500);
    // digitalWrite(LEDPin, HIGH);  //led on
    // printf("...led on\n");
    // delay(500);
  }
  printf("Cycles %lu\n", total/N);
  set_governor(szOldGovernor, NULL);
  return 0;
}

